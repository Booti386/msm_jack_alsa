tgts := msm_jack_alsa
srcs := msm_jack_alsa.c
aux_dir := aux
aux_shared_dir := aux_shared

bin_dir_install := /usr/bin/
aux_dir_install := /etc/
aux_shared_dir_install := /usr/share/

INSTALL ?= install
CP ?= cp

.PHONY: all clean install
all: $(tgts)

clean:
	$(RM) $(tgts)

install: install-tgts install-aux install-aux-shared

install-tgts: $(tgts)
	$(INSTALL) -o 0 -g 0 -m 755 $(tgts) $(bin_dir_install)

install-aux: $(aux_dir)
	$(CP) -a --no-preserve=ownership $(wildcard $(aux_dir)/*) $(aux_dir_install)

install-aux-shared: $(aux_shared_dir)
	$(CP) -a --no-preserve=ownership $(wildcard $(aux_shared_dir)/*) $(aux_shared_dir_install)

msm_jack_alsa: $(srcs)
	$(CC) $(srcs) -o $@ -lasound
