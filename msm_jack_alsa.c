/*
 * Copyright (C) 2015 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h> /* close() */
#include <alsa/asoundlib.h>

#define PULSE_CFG_OVRD "/etc/pulse/client-no-pulse.conf"
#define JACK_STATE_PATH "/sys/devices/virtual/switch/h2w/state"
#define HDS_JACK_CTL_NAME "Headset Jack"
#define HDP_JACK_CTL_NAME "Headphones Jack"
#define POLL_INTERVAL 50000

static int add_jack_switch(snd_ctl_t *snd_ctl, snd_ctl_elem_id_t **jack_switch, snd_ctl_elem_value_t **jack_switch_val, const char *name)
{
	int ret;

	*jack_switch = NULL;
	*jack_switch_val = NULL;

	snd_ctl_elem_id_malloc(jack_switch);
	snd_ctl_elem_id_set_interface(*jack_switch, SND_CTL_ELEM_IFACE_CARD);
	snd_ctl_elem_id_set_name(*jack_switch, name);
	snd_ctl_elem_id_set_device(*jack_switch, 0);
	snd_ctl_elem_id_set_subdevice(*jack_switch, 0);

	ret = snd_ctl_elem_add_boolean(snd_ctl, *jack_switch, 1);
	if(ret < 0
			&& ret != -EEXIST
			&& ret != -EBUSY) {
		fprintf(stderr, "snd_ctl_elem_add_boolean() => %d\n", ret);
		snd_ctl_elem_id_free(*jack_switch);
		*jack_switch = NULL;
		return ret;
	}
	/* Disallow external modifications (otherwise it would be meaningless...) */
	snd_ctl_elem_lock(snd_ctl, *jack_switch);

	snd_ctl_elem_value_malloc(jack_switch_val);
	snd_ctl_elem_value_set_id(*jack_switch_val, *jack_switch);
	return 0;
}

static int remove_jack_switch(snd_ctl_t *snd_ctl, snd_ctl_elem_id_t **jack_switch, snd_ctl_elem_value_t **jack_switch_val)
{
	if(*jack_switch_val)
		snd_ctl_elem_value_free(*jack_switch_val);
	if(*jack_switch) {
		snd_ctl_elem_remove(snd_ctl, *jack_switch);
		snd_ctl_elem_id_free(*jack_switch);
	}
	return 0;
}

int main(int argc, char **argv)
{
	int ret, prog_ret = -1;
	char *pulse_cfg_env;
	int jack_state_fd;
	char old_jack_state = 0, jack_state = 0;
	char *snd_dev_name = "hw:0";
	snd_ctl_t *snd_ctl;
	snd_ctl_elem_id_t *hds_jack_switch, *hdp_jack_switch;
	snd_ctl_elem_value_t *hds_jack_switch_val, *hdp_jack_switch_val;
	int old_hds_enable = -1, hds_enable = 0, old_hdp_enable = -1, hdp_enable = 0;

	pulse_cfg_env = getenv("PULSE_CLIENTCONFIG");
	if(!pulse_cfg_env) {
		ret = setenv("PULSE_CLIENTCONFIG", PULSE_CFG_OVRD, 1);
		if(ret < 0) {
			fprintf(stderr, "Can't start an instance without an active pulse: setenv() => errno=%d\n", errno);
			return -1;
		}
		if(argv[0][0] == '/')
			ret = execv(argv[0], argv);
		else
			ret = execv("/proc/self/exe", argv);
		if(ret < 0) {
			fprintf(stderr, "Unable to self-execute: execv() => errno=%d\nYou should set PULSE_CLIENTCONFIG envvar by hand.\n", errno);
			return -1;
		}
	}
	if(argc == 2)
		snd_dev_name = argv[1];

	jack_state_fd = open(JACK_STATE_PATH, O_RDONLY);
	if(jack_state_fd < 0) {
		fprintf(stderr, "open() => errno=%d\n", errno);
		goto err_open;
	}

	ret = snd_ctl_open(&snd_ctl, snd_dev_name, SND_CTL_NONBLOCK);
	if(ret < 0) {
		fprintf(stderr, "snd_ctl_open() => %d\n", ret);
		goto err_snd_ctl_open;
	}

	ret = add_jack_switch(snd_ctl, &hds_jack_switch, &hds_jack_switch_val, HDS_JACK_CTL_NAME);
	if(ret < 0) {
		fprintf(stderr, "add_jack_switch(HDS_JACK_CTL_NAME=\"%s\") => %d\n", HDS_JACK_CTL_NAME, ret);
		goto err_add_jack_switch_hds;
	}
	ret = add_jack_switch(snd_ctl, &hdp_jack_switch, &hdp_jack_switch_val, HDP_JACK_CTL_NAME);
	if(ret < 0) {
		fprintf(stderr, "add_jack_switch(HDP_JACK_CTL_NAME=\"%s\") => %d\n", HDP_JACK_CTL_NAME, ret);
		goto err_add_jack_switch_hdp;
	}

	while(1) {
		ret = read(jack_state_fd, &jack_state, 1);
		if(ret != 1) {
			fprintf(stderr, "read() => errno=%d\n", errno);
			goto err_read;
		}
		ret = lseek(jack_state_fd, SEEK_SET, 0);
		if(ret < 0) {
			fprintf(stderr, "lseek() => errno=%d\n", errno);
			goto err_lseek;
		}
		if(old_jack_state != 0 /* Initial (invalid) state, so we keep going to initialize everything. */
				&& jack_state == old_jack_state) {
			usleep(POLL_INTERVAL);
			continue;
		}
		switch(jack_state) {
			case '0':
				hds_enable = 0;
				hdp_enable = 0;
				break;
			case '1':
				hds_enable = 1;
				hdp_enable = 0;
				break;
			case '2':
				hds_enable = 0;
				hdp_enable = 1;
				break;
			default:
				fprintf(stderr, "Unknown state read: '%c'.\n", jack_state);
				goto err_invl_data;
		}

		ret = 0;
		/* old_hds_enable and old_hdp_enable are both initialized to -1, so that we make sure that the following code will always be executed at least once (init control values). */
		if(old_hds_enable != hds_enable) {
			snd_ctl_elem_value_set_boolean(hds_jack_switch_val, 0, hds_enable);
			ret = snd_ctl_elem_write(snd_ctl, hds_jack_switch_val);
		}
		if(ret >= 0
				&& old_hdp_enable != hdp_enable) {
			snd_ctl_elem_value_set_boolean(hdp_jack_switch_val, 0, hdp_enable);
			ret = snd_ctl_elem_write(snd_ctl, hdp_jack_switch_val);
		}
		if(ret < 0) {
			fprintf(stderr, "snd_ctl_elem_write() => %d \n", ret);
			goto err_snd_ctl_elem_write;
		}
		old_jack_state = jack_state;
		old_hds_enable = hds_enable;
		old_hdp_enable = hdp_enable;
	}

	prog_ret = 0;

err_snd_ctl_elem_write:
err_invl_data:
err_lseek:
err_read:
	remove_jack_switch(snd_ctl, &hdp_jack_switch, &hdp_jack_switch_val);
err_add_jack_switch_hdp:
	remove_jack_switch(snd_ctl, &hds_jack_switch, &hds_jack_switch_val);
err_add_jack_switch_hds:
	snd_ctl_close(snd_ctl);
err_snd_ctl_open:
	close(jack_state_fd);
err_open:
	return prog_ret;
}
